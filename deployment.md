## 项目部署指南

部署流程：
 * 1.申请微信小程序，
 * 2.开通云开发并上传云函数。
 * 3.配置ocr服务。
 * 4.申请一个天行数据的接口key。
 * 5.导入项目，并开始编译

### 一、 申请微信小程序，

申请地址：https://mp.weixin.qq.com/
请按照官方流程操作

### 二、 你需要配置你的云开发
1· 在储存里建立一个images的文件夹

![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/200214_a33be369_1791536.png "屏幕截图.png")

2· 新建你的云环境Id，并且配置
路径:/miniprogram/app.js
配置微信云开发环境ID
wx.cloud.init({
  env: '你的环境ID',
  traceUser: true,
})


### 三、 配置ocr服务。

如果使用微服务的方法：
在https://developers.weixin.qq.com/community/servicemarket/detail/000ce4cec24ca026d37900ed551415
购买即可

### 四、 申请谣言鉴定接口（目前这种方法并不是很好，后期考虑换成后台保存的方法。）

地址：https://www.tianapi.com/apiview/171

配置apiKey：
/miniprogram/pages/index/index.js
checkingRumors方法key=字段

### 五、 导入项目，下载微信开发者工具（https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html）

配置appid：
路径:/project.config.json
配置你的appid导入项目

### 然后在微信公众平台后台填加一个微信同声传译的插件
![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/203515_c3f03382_1791536.png "屏幕截图.png")



### 导入项目，你就可以使用了。

嘻嘻(*^__^*) 嘻嘻